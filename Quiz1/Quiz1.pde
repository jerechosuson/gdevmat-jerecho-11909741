void setup()
{
  size(1280, 720, P3D); // window size
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

//a function that gets called in every frame
void draw()
{
  background(0); //sets background color of the whole window
  circle(0, 0, 2); // draws a circle around x,y with thickness
  strokeWeight(1); // alters the line thickness beneath
  color white = color (255, 255, 255); // color variable
  fill(white); // fills with the color selected
  stroke(white); // strokes will inline with the color selected
  line(300, 0, -300, 0); //draw a line x1, y1, x2, y2
  line(0, -300, 0, 300);
  

  for (int i = -300; i <= 300; i += 10) //Cartesian plane
  {
    line(i, -2, i, 2);
    line(-2, i, 2, i);
  }
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  drawSinewave();
  
}

void drawCartesianPlane()
{
  strokeWeight(1); 
  color white = color (255, 255, 255); 
  fill(white);
  stroke(white); 
  line(300, 0, -300, 0); 
  line(0, -300, 0, 300);
}
void drawLinearFunction()
{
  color pink = color(255, 55, 255);
  fill(pink);
  noStroke();

  for (int x = -200; x <=200; x++)
  {
    circle(x, x + 2, 5);
  }
}
void drawQuadraticFunction()
{
  color lightBlue = color (55, 255, 255); 
  fill(lightBlue);
  stroke(lightBlue);
  noStroke();
  for (float x = -300; x <= 300; x += 0.1f)
  {
    circle(x * 10, (float)Math.pow(x, 2) + (2 * x) - 5, 5);
  }
}
void drawCircle()
{
  color yellow = color (255, 255, 55); 
  fill(yellow);
  stroke(yellow);
  float radius = 45; //radius of a circle
  for (int x = 0; x <= 360; x++) //circle diameter
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 5);
  }
}

void drawSinewave()
{
  color salmon = color (255, 145, 155); 
  fill(salmon);
  stroke(salmon);
  strokeWeight(2);
  float a = 0.0;  
  float b = 0.0;
float inc = TWO_PI/35.0;
float tnc = TWO_PI/35.0;
float previ_x = 0, previ_y = 0, x, y;

//Stationary sinewave
for(int i=0; i<240; i=i+4) {
  x = i;
  y = 0 + sin(a) * 55.0;
  line(previ_x, previ_y, x, y); //right waves
  y = 0 + sin(b) * 55.0;
  line(-previ_x, -previ_y, -x, -y); //left waves
  previ_x = x;
  previ_y = y;
  a = a + inc; //cleaning lines for right waves
  b = b + tnc; //cleaning lines for the left waves

}
}
