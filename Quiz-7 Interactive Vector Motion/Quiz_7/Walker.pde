public class walker
{
  public PVector momentum = new PVector();
  public PVector path = new PVector();
  public PVector pose = new PVector();
  public PVector velocity = new PVector();
  
  public float scale = random(3, 20);
  public float velocityRate = 8.5;

  PVector randomPlacement = new PVector(random(Window.left, Window.right + 10), random(Window.bottom, Window.top + 10));

  public walker() 
  {
  }
  
 public void update ()
 { 
   PVector cursor = new PVector( mouseX - Window.windowWidth / 2, -(mouseY - Window.windowHeight / 2));
    PVector liveDirection = PVector.sub(cursor, this.pose);
   this.path = liveDirection;
   path.normalize();
   pose.add(velocity);
   velocity.add(momentum);
   velocity.limit(velocityRate);
   momentum = path;
   momentum.mult(0.2);
   
    stroke(random(100,255), random(100, 255), random(100,255)); // color variable
      // strokes will inline with the color selected
 }
  public void render()
  {
   circle(randomPlacement.x, randomPlacement.y, scale);
   pose = randomPlacement;
  }
}
