public static class Window
{
    public static int windowWidth = 1080;
    public static int windowHeight = 720;
    public static int w = windowWidth/2;
    public static int h = windowHeight/2;
    public static int top = h;
    public static int bottom = -h;
    public static int left = -w;
    public static int right = w;
    public static float eyeZ = -(windowHeight/2.0) / tan(PI*30.0 / 180.0);
}
void moveAndBounce()
{
   //add speed to the current positon
  position.add(speed);
  //conditions for bouncing
  if ((position.x > Window.right) || (position.x < Window.left))
  {
    speed.x *=-1;
  }
  if ((position.y > Window.top) || (position.y < Window.bottom))
  {
    speed.y *=-1;
  }
  //render circle
  fill(182, 52, 100);
  circle(position.x, position.y, 40);
  strokeWeight(random(80,10)); // alters the line thickness beneath
  stroke(random(100,255), random(100, 255), random(100,255)); // color variable
  // strokes will inline with the color selected
}
