void setup ()
{
  size(1280, 720, P3D); // window size
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
}

Walker myWalker = new Walker(); //instance for walker class
Walker xWalker = new Walker();

void draw()
{
  int rng = int(random(8)); // generates random value from 0-5
  println(rng);
  myWalker.randomWalk();
  myWalker.render(); 
}
