walker[] circl = new walker [10];
walker walker = new walker();

PVector w = new PVector(0.15, 0);
PVector g = new PVector(0, -0.4);

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
 //Mass and Scales of Circles
 for (int i = 0; i < circl.length; i++)
 {
   circl[i] = new walker();
   circl[i].mass = 10 - i;
   circl[i].scale = circl[i].mass * 15;
   circl[i].render();
   circl[i].color1 = random(60, 255);
   circl[i].color2 = random(60, 255);
   circl[i].color3 = random(60, 255);
   strokeWeight(random(1, 3));
   stroke(random(100,255), random(100, 255), random(100,255));
 }
}

void draw()
{
  background(80);
  //Set of Frame of Circles
   for (int i = 0; i < circl.length; i++)
 {
   circl[i].checkEdges();
   circl[i].applyForce(g);
   circl[i].applyForce(w);
   circl[i].update();
   circl[i].render();
 }
}
