public class walker 
{
  public float mass = 1;
  public float scale= 1;
  public float color1;
  public float color2;
  public float color3;
  public float stroke;

  public float velocityRate = 10;
  public PVector velocity= new PVector();
  public PVector momentum = new PVector();
  public PVector pose = new PVector(-500, 200);
  
  public walker()
  {
    
  }

  //Circle's Velocity and Momentum
  public void update()
  {
    this.velocity.limit(velocityRate);
    this.pose.add(this.velocity);
    this.velocity.add(this.momentum);
    this.momentum.mult(0);
      
  }
  
   //Circle's force
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.momentum.add(f); 
  }
  
  //render
  public void render()
  {
    circle(pose.x, pose.y, scale);
    fill(color1, color2, color3);
  }

  //Circle on edges will bounce off
  public void checkEdges()
  {
    if (this.pose.x < Window.left)
    {
      this.velocity.x *= -1;
    }
    else if (this.pose.x > Window.right)
    {
      this.pose.x = Window.right;
      this.velocity.x *= -1;
    }
    
    if (this.pose.y < Window.bottom)
    {
      this.pose.y = Window.bottom;
       this.velocity.y *= -1;
    }
    else if (this.pose.y > Window.top)
    {
      this.velocity.y *= -1;
    }
    }
  }

  
