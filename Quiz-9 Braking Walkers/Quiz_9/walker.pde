public class walker 
{
  public float scale= 1;
  public float mass = 1;
  public float color1;
  public float color2;
  public float color3;
  public float stroke;
  
  public float velocityRate = 10;
  public PVector velocity= new PVector();
  public PVector momentum = new PVector();
  public PVector pose = new PVector(-500, 200);
  
  public walker()
  {
    
  }
  
   //Circle's force
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.momentum.add(f); 
  }
  
  //Circle's Velocity and Momentum
  public void update()
  {
    this.velocity.limit(velocityRate);
    this.pose.add(this.velocity);
    this.velocity.add(this.momentum);
    this.momentum.mult(0);
  }
  
  public void render()
  {
    circle(pose.x, pose.y, scale);
    fill(color1, color2, color3);
   
  }
}
  
