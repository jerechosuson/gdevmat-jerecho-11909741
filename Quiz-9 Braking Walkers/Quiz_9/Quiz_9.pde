walker walker = new walker();
walker[] circl = new walker [8];

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
  int posY = 0;
 //Mass and Scales of Circles
 for (int i = 0; i < circl.length; i++)
 {
   posY = 2 * (Window.windowHeight / 8) * (i - 4);
   circl[i] = new walker();
   circl[i].mass = 10 - i;
   circl[i].scale = circl[i].mass * 8;
   circl[i].color1 = random(60, 255);
   circl[i].color2 = random(60, 255);
   circl[i].color3 = random(60, 255);
   strokeWeight(random(1, 3));
   stroke(random(100,255), random(100, 255), random(100,255));
   circl[i].pose = new PVector(-480, posY);
   circl[i].render();
 }
}

void draw()
{ 
  background(80);
  //Set of Frame of Circles
   for (walker w : circl)
 {
 stroke(random(100,255), random(100, 255), random(100,255));
 line(0, Window.top, 0, Window.bottom);
   float mew = 0.01f; 
       if (w.pose.x > 0)
    {
      mew = 0.4f;
    }
   float normal = .94;
   float friction = mew  * normal;
   PVector mo = new PVector(0.2, 0); 
   PVector fr = w.velocity.copy();
   fr.mult(-1);
   fr.normalize();
   fr.mult(friction);
   w.applyForce(fr);
   w.applyForce(mo);
   w.update();
   w.render();
    println(mew);
 }
}

//renewal
void mousePressed()
{
   setup();
}
