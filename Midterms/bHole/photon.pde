class Photon {
  PVector pos, vel;
  ArrayList<PVector> history;
  boolean stopped;
  float theta;
  Photon(float x, float y) {
    this.pos = new PVector(x, y);
    this.vel = new PVector(-c, 0);
    this.history = new ArrayList<PVector>();
    this.stopped = false;
    this.theta = 0;
  }

  void stop() {
    this.stopped = true;
  }

  void update() {
    if (!this.stopped) {
      //if (frameCount % 8 == 0) {
      this.history.add(this.pos.copy());
      //}
      PVector deltaV = this.vel.copy();
      deltaV.mult(dt);
      this.pos.add(deltaV);
    }

    if (this.history.size() > 200) {
      this.history.remove(0);
    }
  }

  void show() {
    strokeWeight(random(5,25)); // alters the line thickness beneath
    stroke(random(180,255), random(180, 255), random(180,255)); // color variable
    // strokes will inline with the color selected
    point(this.pos.x, this.pos.y);

    strokeWeight(0);
    noFill();
    beginShape();
    for (PVector v : this.history) {
      vertex(v.x, v.y);
    }

    endShape();
  }
}
