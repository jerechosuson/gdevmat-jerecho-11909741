void setup ()
{
  size(1080, 720, P3D); // window size
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}
PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowWidth / 2);
  return new PVector(x, y);
}

void draw()
{
  background(130);
  drawOutter();
  
  //render the lightsaber
  fill(182, 52, 100);
  rect(15, 16, -15, -15);
  strokeWeight(random(23,32));
  stroke(random(55/85,255), random(0, 0), random(0,0)); // color variable
  // strokes will inline with the color selected
  PVector mouse = mousePos();
  line(0,0, mouse.x, mouse.y);
}
void drawOutter()
{
  strokeWeight(random(34,42));
  stroke(random(245,255), random(245, 0), random(245,0)); // color variable
  // strokes will inline with the color selected
  PVector mouse = mousePos();
  line(0,0, mouse.x, mouse.y);
}
