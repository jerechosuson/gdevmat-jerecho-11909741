walker walker = new walker();
walker[] circl = new walker[10];

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
  
   for (int i = 0; i < circl.length; i++)
 {
   circl[i] = new walker();
   circl[i].mass = random(5, 12);
   circl[i].scale = circl[i].mass * 10;
   circl[i].color1 = random(60, 255);
   circl[i].color2 = random(60, 255);
   circl[i].color3 = random(60, 255);
   strokeWeight(random(1, 3));
   stroke(random(100,255), random(100, 255), random(100,255));
 }
}    

void draw()
{
   background(255); 
for (walker walker : circl)
{
 walker.update();
 walker.render();
  for (walker walker1 : circl)
  {
   if (walker != walker1)
  {
   walker.applyForce(walker1.calculateAttraction(walker));
  }
 }  
}
}

void mousePressed()
{
   setup();
}
