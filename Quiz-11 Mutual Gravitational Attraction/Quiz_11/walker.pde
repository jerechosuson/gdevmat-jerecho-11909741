public class walker 
{
  public float scale= 10;
  public float mass = 1;
  public float color1;
  public float color2;
  public float color3;
  public float stroke;
  
  public float velocityRate = 17;
  public PVector velocity= new PVector();
  public PVector momentum = new PVector();
  public PVector pose = new PVector();
  
  PVector randomPose = new PVector(random(Window.left, Window.right + 1),
  random(Window.bottom, Window.top + 1));
  public float gravitationalConstant = 4.6;
  
  public walker()
  {
  }
  
   //Circle's force
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.momentum.add(f); 
  }
  
  //Circle's Velocity and Momentum
  public void update()
  {
    this.velocity.limit(velocityRate);
    this.pose.add(this.velocity);
    this.velocity.add(this.momentum);
    this.momentum.mult(0);
  }
  
  public PVector calculateAttraction(walker walker)
  {
    PVector force = PVector.sub(this.pose, walker.pose);
    float dis = force.mag();
    force.normalize();
    dis = constrain(dis, 8, 22);
    float str = (this.gravitationalConstant * this.mass * walker.mass) / (dis * dis);
    force.mult(str);
    return force;
  }
  
  public void render()
  {
    fill(color1, color2, color3, 105);
    circle(randomPose.x, randomPose.y, scale);
    pose = randomPose;
  }
}
  
