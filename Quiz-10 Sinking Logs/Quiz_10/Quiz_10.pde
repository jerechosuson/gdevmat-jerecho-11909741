walker mywalker = new walker();
walker[] circl = new walker[10];
fluid ocean = new fluid (0, -100,Window.right,Window.bottom, 0.1f);

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
 int posX = 0;
 //Mass and Scales of Circles
 for (int i = 0; i <circl.length; i++)
 {
   posX = 2 * (Window.windowHeight / 10) * (i - 5);
   circl[i] = new walker();
   circl[i].mass = random(5, 12);
   circl[i].scale =circl[i].mass * 10;
   circl[i].color1 = random(60, 255);
   circl[i].color2 = random(60, 255);
   circl[i].color3 = random(60, 255);
   circl[i].pose = new PVector(posX, 380);
   strokeWeight(random(1, 1.8));
   stroke(random(100,255), random(100, 255), random(100,255));
 }
 }

void draw()
{
  background(240);
  ocean.render();
   for (int i = 0; i < circl.length; i++)
 {
   float c = 0.1f;
   float normal = 1;
   float frictionMag = c * normal;
   PVector friction = circl[i].velocity.copy();  
   PVector w = new PVector(0.1, 0);
   PVector g = new PVector(0, -0.15 * circl[i].mass);
   if(ocean.collision(circl[i]))


  {
    PVector dragForce = ocean.dragforceCalc(circl[i]);
    circl[i].applyForce(dragForce);
    w.x = 0;
    w.y = 0;
    if(circl[i].pose.y <= Window.bottom)
  {
    circl[i].velocity.y *= -1;
    circl[i].pose.y = Window.bottom;
  }

  }
   circl[i].applyForce(friction.mult(-1).normalize().mult(frictionMag));
   circl[i].applyForce(g);
   circl[i].applyForce(w);
   circl[i].update();
   circl[i].render();
 }
 
}
void mousePressed()
{
   setup();
}
