public class fluid
{
  public float x;
  public float y;
  public float z;
  public float veticalValue;
  public float horizonalValue;
  
  
  public fluid(float x, float y, float horizonalValue, float veticalValue, float z)
  {
    this.x = x;
    this.y = y;
    this.z = z;
    this.veticalValue = veticalValue;
    this.horizonalValue = horizonalValue;
    

    
    
  }
  
  public void render()
  {
    fill(28, 130, 176);
    beginShape();
    vertex(x - horizonalValue, y, 0);
    vertex(x + horizonalValue, y, 0);
    vertex(x + horizonalValue, y + veticalValue, 0);
    vertex(x - horizonalValue, y + veticalValue, 0);
    endShape();
   }
   
  public PVector dragforceCalc(walker walker)
  {
    float momentum = walker.velocity.mag();
    float dragMag = this.z * momentum * momentum;
    
    PVector dragForce = walker.velocity.copy().mult(-1);
    
    dragForce.normalize();
    dragForce.mult(dragMag);
    return dragForce;
  }
  
  public boolean collision(walker walker)
  {
   PVector pos = walker.pose;
    return pos.x > this.x - this.horizonalValue &&
           pos.x < this.x + this.horizonalValue &&
           pos.y < this.y;
  }
}
