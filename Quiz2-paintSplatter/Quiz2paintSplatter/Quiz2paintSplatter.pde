
void setup()
{
  size(1280, 720, P3D); // window size
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background(0);
}

//a function that gets called in every frame
void draw()
{

  float circl = randomGaussian();
  float sd = random(420);
  float mean = width/42;
  circl = (circl * sd) + mean;

  drawFade();
  
  strokeWeight(random(32,72)); // alters the line thickness beneath
  stroke(random(100,255), random(100, 255), random(100,255)); // color variable
  // strokes will inline with the color selected
  fill(13,15);

  ellipse(circl, random(-330,520),random(18,32),random(18,32));
  int circles = int(circl);
  if (circles >= 300)
  {
  background(0);
  }
  }
  
