public class Walker
{
  public float x;
  public float y;
  public float tx = 0, ty = 10000;

  
  void render()
  {
    circle(x, y, 5);
  }
  
  void perlinWalk()
  {
    x = map(noise(tx), 0, 1, -640, 640);
    y = map(noise(ty), 0, 1, -560, 560);
 
    strokeWeight(random(32,72)); // alters the line thickness beneath
    stroke(random(100,255), random(100, 255), random(100,255)); // color variable
      // strokes will inline with the color selected
      
    
    tx += 0.01f;
    ty += 0.01f;

  }
}
    
