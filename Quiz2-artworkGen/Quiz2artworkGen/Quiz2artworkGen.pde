
void setup()
{
  size(1280, 720, P3D); // window size
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background(255);
}
Walker perlinWalker = new Walker();
float t = 0;
//a function that gets called in every frame
void draw()
{
  
  fill(0);
  perlinWalker.render();
  perlinWalker.perlinWalk();
}
